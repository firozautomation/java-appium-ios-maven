package steps;

import io.cucumber.java.en.When;
import pages.PabloPicassoPage;
import pages.Page;

public class PaintingSteps extends Page {

    @When("^the user views more info for Guernica painting$")
    public void iViewMoreInfoForGuernicaPainting() {
        instanceOf(PabloPicassoPage.class).viewMoreInfoGuernica();
    }
}
package utils.appium;

import io.appium.java_client.ios.IOSDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.drivers.IOSAppDriver;
import java.io.IOException;

public class DriverController {

    public static DriverController instance = new DriverController();

    IOSDriver iosDriver;

    private static Logger log = LogManager.getLogger(DriverController.class);

    public void startAppDriver(String platformVersion, String deviceName, int wdaLocalPort) throws IOException {
        if (instance.iosDriver != null) return;
        instance.iosDriver = IOSAppDriver.loadDriver(platformVersion, deviceName, wdaLocalPort);
    }

    public void stopAppDriver() {
        if (instance.iosDriver == null) return;

        try {
            instance.iosDriver.quit();
        } catch (Exception e) {
            log.error(e + ":: AndroidDriver stop error");
        }

        AppiumServer.stop();
        instance.iosDriver = null;
        log.debug(":: AndroidDriver stopped");
    }
}